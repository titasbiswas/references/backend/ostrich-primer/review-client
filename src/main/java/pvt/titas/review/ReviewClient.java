package pvt.titas.review;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;
import pvt.titas.review.config.ReviewClientConfiguration;
import pvt.titas.review.resource.ReviewClientTestResource;


public class ReviewClient extends Application<ReviewClientConfiguration> {


    @Override
    public void run(ReviewClientConfiguration config, Environment env) throws Exception {

        ReviewClientTestResource reviewClientTestResource = new ReviewClientTestResource(config, env);
        env.jersey().register(reviewClientTestResource);


    }

    @Override
    public void initialize(Bootstrap<ReviewClientConfiguration> bootstrap) {
    }

    public static void main(String[] args) throws Exception {
        new ReviewClient().run(args);

    }




}
