package pvt.titas.review.resource;

import com.bazaarvoice.ostrich.ServicePool;
import com.bazaarvoice.ostrich.discovery.zookeeper.ZooKeeperHostDiscovery;
import com.bazaarvoice.ostrich.dropwizard.healthcheck.ContainsHealthyEndPointCheck;
import com.bazaarvoice.ostrich.pool.ServiceCachingPolicy;
import com.bazaarvoice.ostrich.pool.ServiceCachingPolicyBuilder;
import com.bazaarvoice.ostrich.pool.ServicePoolBuilder;
import com.bazaarvoice.ostrich.retry.ExponentialBackoffRetry;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.setup.Environment;
import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import pvt.titas.review.client.ReviewService;
import pvt.titas.review.client.ReviewServiceFactory;
import pvt.titas.review.config.ReviewClientConfiguration;
import pvt.titas.review.model.Review;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Path("/review-client-test")
@Slf4j
public class ReviewClientTestResource {

    private static final String NAME = "jersey-lient";
    private ServicePool<ReviewService> pool;
    private CuratorFramework curator;

    public ReviewClientTestResource(ReviewClientConfiguration config, Environment env){
        curator = config.getZooKeeperConfiguration().newCurator();
        curator.start();

        // Connection caching is optional, but included here for the sake of demonstration.
        ServiceCachingPolicy cachingPolicy = new ServiceCachingPolicyBuilder()
                .withMaxNumServiceInstances(10)
                .withMaxNumServiceInstancesPerEndPoint(1)
                .withMaxServiceInstanceIdleTime(5, TimeUnit.MINUTES)
                .build();

        JerseyClientConfiguration httpClientConfiguration = config.getHttpClientConfiguration();
        JerseyClientBuilder builder = new JerseyClientBuilder(env);
        Client httpClient = builder.using(config.getHttpClientConfiguration()).build(NAME);

        MetricRegistry metrics = new MetricRegistry();
        ReviewServiceFactory serviceFactory = new ReviewServiceFactory(httpClient);
        pool = ServicePoolBuilder.create(ReviewService.class)
                .withServiceFactory(serviceFactory)
                .withHostDiscovery(new ZooKeeperHostDiscovery(curator, serviceFactory.getServiceName(), metrics))
                .withMetricRegistry(metrics)
                .withCachingPolicy(cachingPolicy)
                .build();

        HealthCheckRegistry healthChecks = new HealthCheckRegistry();
        healthChecks.register("review-consumer", ContainsHealthyEndPointCheck.forPool(pool));

    }

    @GET
    public void testReviewClient() throws IOException {

        operate();

        //Closeables.close(pool, true);
        //Closeables.close(curator, true);
    }

    private void operate() {
        List<Review> reviews = pool.execute(new ExponentialBackoffRetry(5, 50, 1000, TimeUnit.MILLISECONDS),
                service -> {
                    return service.getAllReview();
                });
        log.info("Reviews retrieved from service -->"+reviews);
        reviews.forEach(e-> log.info(String.valueOf(e)));

        Random random = new Random();
        Integer randomId = random.nextInt(10);
        Review review = pool.execute(new ExponentialBackoffRetry(5, 50, 1000, TimeUnit.MILLISECONDS),
                service -> {
                    return service.getReviewById(randomId);
                });
        log.info("Review retrieved from service for id :"+randomId+" is "+review);
    }
}
