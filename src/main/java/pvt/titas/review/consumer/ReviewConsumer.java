package pvt.titas.review.consumer;

import com.bazaarvoice.ostrich.ServicePool;
import com.bazaarvoice.ostrich.discovery.zookeeper.ZooKeeperHostDiscovery;
import com.bazaarvoice.ostrich.dropwizard.healthcheck.ContainsHealthyEndPointCheck;
import com.bazaarvoice.ostrich.pool.ServiceCachingPolicy;
import com.bazaarvoice.ostrich.pool.ServiceCachingPolicyBuilder;
import com.bazaarvoice.ostrich.pool.ServicePoolBuilder;
import com.bazaarvoice.ostrich.retry.ExponentialBackoffRetry;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.google.common.io.Closeables;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.configuration.ConfigurationException;
import io.dropwizard.configuration.ConfigurationFactory;
import io.dropwizard.configuration.YamlConfigurationFactory;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.util.JarLocation;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.curator.framework.CuratorFramework;
import pvt.titas.review.client.ReviewService;
import pvt.titas.review.client.ReviewServiceFactory;
import pvt.titas.review.config.ReviewClientConfiguration;
import pvt.titas.review.model.Review;

import javax.validation.Validation;
import javax.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ReviewConsumer {

    private final ServicePool<ReviewService> _reviewServicePool;

    public ReviewConsumer(ServicePool<ReviewService> reviewServicePool) {
        _reviewServicePool = reviewServicePool;
    }

    public static void main(String[] args) throws Exception {
        Namespace parsedArgs = parseCommandLine(args);
        // Connect to ZooKeeper
        ReviewClientConfiguration config = loadConfigFile(parsedArgs.getString("config"));
        CuratorFramework curator = config.getZooKeeperConfiguration().newCurator();
        curator.start();

        // Connection caching is optional, but included here for the sake of demonstration.
        ServiceCachingPolicy cachingPolicy = new ServiceCachingPolicyBuilder()
                .withMaxNumServiceInstances(10)
                .withMaxNumServiceInstancesPerEndPoint(1)
                .withMaxServiceInstanceIdleTime(5, TimeUnit.MINUTES)
                .build();

        JerseyClientConfiguration httpClientConfiguration = config.getHttpClientConfiguration();
        MetricRegistry metrics = new MetricRegistry();
        ReviewServiceFactory serviceFactory = new ReviewServiceFactory(httpClientConfiguration, metrics);
        ServicePool<ReviewService> pool = ServicePoolBuilder.create(ReviewService.class)
                .withServiceFactory(serviceFactory)
                .withHostDiscovery(new ZooKeeperHostDiscovery(curator, serviceFactory.getServiceName(), metrics))
                .withMetricRegistry(metrics)
                .withCachingPolicy(cachingPolicy)
                .build();

        // If using Yammer Metrics or running in Dropwizard (which includes Yammer Metrics), you may want a health
        // check that pings a service you depend on. This will register a simple check that will confirm the service
        // pool contains at least one healthy end point.
        HealthCheckRegistry healthChecks = new HealthCheckRegistry();
        healthChecks.register("calculator-user", ContainsHealthyEndPointCheck.forPool(pool));

        ReviewConsumer consumer = new ReviewConsumer(pool);
        consumer.operate();

        Closeables.close(pool, true);
        Closeables.close(curator, true);
    }

    private void operate() {
        List<Review> reviews = _reviewServicePool.execute(new ExponentialBackoffRetry(5, 50, 1000, TimeUnit.MILLISECONDS),
                service -> {
                    return service.getAllReview();
                });
        log.info("Reviews retrieved from service -->"+reviews);
        reviews.forEach(e-> log.info(String.valueOf(e)));

        Random random = new Random();
        Integer randomId = random.nextInt(10);
        Review review = _reviewServicePool.execute(new ExponentialBackoffRetry(5, 50, 1000, TimeUnit.MILLISECONDS),
                service -> {
                    return service.getReviewById(randomId);
                });
        log.info("Review retrieved from service for id :"+randomId+" is "+reviews);
    }


    private static Namespace parseCommandLine(String[] args) throws ArgumentParserException {
        String usage = "java -jar " + new JarLocation(ReviewConsumer.class);
        ArgumentParser argParser = ArgumentParsers.newArgumentParser(usage).defaultHelp(true);
        argParser.addArgument("config").nargs("?").help("yaml configuration file");
        return argParser.parseArgs(args);
    }

    private static ReviewClientConfiguration loadConfigFile(String configFile)
            throws IOException, ConfigurationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        ConfigurationFactory<ReviewClientConfiguration> configFactory = new YamlConfigurationFactory<>(
                ReviewClientConfiguration.class, validator, Jackson.newObjectMapper(), "review-consumer"
        );
        if (configFile != null) {
            return configFactory.build(new File(configFile));
        } else {
            return configFactory.build();
        }
    }
}
